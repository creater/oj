#oj
PowerOJ is a online judge for ACM/ICPC or OI.

Online Judge is a web service which can compile user's source code and run in limition. 

Judge use predefined data files as stdin and get user's stdout as files, compare with the standard output data to judge the user's solution.

<table>
  <tbody>
    <tr><td> JAVA Framework  </td> <td> JFinal        </td></tr>
    <tr><td> Template Engine </td> <td> FreeMarker    </td></tr>
    <tr><td> JS Framework    </td> <td> jQuery        </td></tr>
    <tr><td> CSS Framework   </td> <td> Bootstrap 2   </td></tr> 
    <tr><td> Data Base       </td> <td> MySQL         </td></tr>
    <tr><td> Web Server      </td> <td> Tomcat 7      </td></tr> 
    <tr><td> OS              </td> <td> Linux/Windows </td></tr>
  </tbody>
</table>